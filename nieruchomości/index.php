<?php
	include('send.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>DOMIAUTO | Formularz ubezpieczenia nieruchomości</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    
</head>
<body>
	<div class="container-fluid nav" id="nav">
		<nav class="navbar navbar-light navbar-expand ">
			<a href="http://domiauto.pl" class="navbar-brand">
				<img src="../img/domiauto.png" class="img-fluid brand">
			</a>
    		<div class="nav navbar-nav">
    			<a href="http://domiauto.pl" class="nav-item ml-auto">
    				<img src="../img/home.svg" alt="error" class="img-fluid home">
    			</a>	
    		</div>
    		
		</nav>
		
	</div>
	<div class="call" data-placement="bottom" data-fallbackPlacement="counterclockwise" data-trigger="click" data-html="true" data-container=".call" data-toggle="popover" data-content="Zadzwoń do nas:<br> 
				570-752-100 <br>
				577-123-870" >
				<img src="../img/telefon.png" alt="" class="call_img" >
		</div>

	<div class="container-fluid main">
		<h1 class="welcome">
			Pokażemy Ci jak w trzech
			<br>
			prostych krokach ubezpieczyć
			<br>
			swoją nieruchomość
		</h1>
		<div class="list">
			<p class="one">
				1. Podaj nam swoje podstawowe dane
			</p>
			<p class="two">
				2. Prześlemy Ci 3 najlepsze oferty
			</p>
			<p class="three">
				3. Wybierz ofertę i podpisz polisę
			</p>

			<div class="submit">
				<a class="site-link" href="#section2">
						Zaczynajmy
					</a>
			</div>
		</div>
		
	</div>
	
	<div class="container-fluid section2" id="section2">
		
		<div class="n-d top">
			<a href="#nav" class="a-top">
				<img src="../img/top.png" class="img-fluid" alt="">
			</a>
		</div>
		<div class="form">
			<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
				<h1 class="tac">Wypełnij formularz</h1>
				<br>
				<label for="post" class="col-6 right name">Kod pocztowy:</label>
				<input type="text" name="post" id="post"  placeholder="__-___" value="<?=$post?>" />
				<label class="col-6 right name">Miejscowość:</label>
				<input type="text" name="location" id="location" value="<?=$location?>">
				<label class="col-6 right name">Ulica:</label>
				<input type="text" name="street" id="street" value="<?=$street?>">
				<label class="col-6 right name">Nr domu/lokalu:</label>
				<input type="number" name="nr_dom" class="short" value="<?=$nr_domu?>">
				/
				<input type="number" name="nr_lokal" class="short" value="<?=$nr_lokalu?>">
				<label class="tac name">Budynek w budowie:</label>
				<?=
					$tak = $nie = "";
					if($budowa == "tak"){
						$tak = "checked";
					} else if($budowa == "nie"){
						$nie = "checked";
					}
				?>
				<div class="container-fluid tac">
					<input type="radio" name="budowa" value="tak" class="btn-input btn-yes" id="yes" <?=$tak?>>
					<label for="yes" class="tak">Tak</label>
					<input type="radio" name="budowa" value="nie" class="btn-input btn-no" id="no" <?=$nie?>>
					<label for="no" class="nie">Nie</label>
				</div>
				<label class="tac name">Rodzaj nieruchmości:</label>
				<?=
					$radio1 = $radio2 = "";
					if($type == "mieszkanie"){
						$radio1 = "checked";
					} else if($type == "dom"){
						$radio2 = "checked";
					}
				?>
				<div class="container-fluid tac">
					<input type="radio" name="type" value="mieszkanie" class="btn-input btn-miesz <?=$radio1?>" id="mieszkanie" <?=$radio1?>>
					<label for="mieszkanie" class="type miesz">Mieszkanie</label>
					<input type="radio" name="type" value="dom" class="btn-input btn-dom" id="dom" <?=$radio2?>>
					<label for="dom" class="type dom">Dom</label>
				</div>
					<label class="no-disp col-6 right name">Piętro:</label>
				<select class="no-disp" name="floor" id="floor">
					<?=
						$option = $option1 = $option2 = $option3 = "";
						if($floor == ""){
							$option = "selected";
						} else if($floor == "parter"){
							$option1 = "selected";
						} else if($floor == "pomiędzy"){
							$option2 = "selected";
						} else if($floor == "ostatnie"){
							$option3 = "selected";
						}
					?>
					<option value="" <?=$option?>>--wybierz--</option>
					<option value="parter" <?=$option1?>>parter</option>
					<option value="pomiędzy" <?=$option2?>>pomiędzy</option>
					<option value="ostatnie" <?=$option3?>>ostatnie</option>
				</select>
				<label class="tac name">Konstrukcja budynku:</label>
				<div class="container-fluid tac">
				<?=
					$cegla = $drewno ="";
					if($constr == "murowana"){
						$cegla = "checked";
					} else if($constr == "drewniana"){
						$drewno = "checked";
					}
				?>
					<input type="radio" name="constr" value="murowana" class="btn-input btn-mur" id="mur" <?=$cegla?>>
					<label for="mur" class="mur">Murowana</label>
					<input type="radio" name="constr" value="drewniana" class="btn-input btn-wood" id="drewno" <?=$drewno?>>
					<label for="drewno" class="wood">Drewniana</label>
				</div>
				<label class=" col-6 right name">Powierzchnia (m<span class="sup">2</span>):</label>
				<input type="number" name="space" value="<?=$space?>" id="space">
				<label class="col-6 right name">Rok budowy:</label>
				<input type="number" name="constr_year" value="<?=$constr_year?>" id="constr_year">
				
				<label class="col-6 right name">Forma własności:</label>
				<select name="ownership" id="ownership">
				<?=
					$wlasnosc = $wlasnosc1 = $wlasnosc2 = $wlasnosc3 = $wlasnosc4 = "";
					if($ownership == ""){
						$wlasnosc = "selected";
					} else if($ownership == "własność"){
						$wlasnosc1 = "selected";
					} else if($ownership == "Spółdzielcze"){
						$wlasnosc2 = "selected";
					} else if($ownership == "Najem"){
						$wlasnosc3 = "selected";
					} else if($ownership == "Inna"){
						$wlasnosc4 = "selected";
					}
				?>
					<option value="" <?=$wlasnosc?>>--wybierz--</option>
					<option value="własność" <?=$wlasnosc1?>>Własność</option>
					<option value="Spółdzielcze" <?=$wlasnosc2?>>Spółdzielcze</option>
					<option value="Najem" <?=$wlasnosc3?>>Najem</option>
					<option value="Inna" <?=$wlasnosc4?>>Inna</option>
				</select>
				<label class="col-6 right name">Obecny ubezpieczyciel:</label>
				<input type="text" name="ubezpieczyciel" value="<?=$ubezp?>" id="ubezpieczyciel">
				<h1 class="tac">Okres ubezpieczenia</h1>
				<label class="col-6 right name">Data rozpoczęcia:</label>
				<input type="date" name="start" value="<?=$start?>" id="start">
				<h1 class="tac">Dane Kontaktowe</h1>
				<label for="name" class="col-6 right name">Imię:</label>
				<input type="text" name="name" id="name" value="<?=$name?>">
				<label for="surname" class="col-6 right name">Nazwisko:</label>
				<input type="text" name="surname" id="surname" value="<?=$surname?>">
				<label for="email" class="col-6 right name">E-mail:</label>
				<input type="email" name="email" id="email" value="<?=$email?>">
				<span class="error"><?= $Err2 ?></span>
				<label for="mobile" class="col-6 right name">Numer telefonu:</label>
				<input type="tel" name="phone" id="mobile" value="<?=$phone?>">
				<span class="error"><?= $Err2 ?></span>
				<label for="pesel" class="col-6 right name">PESEL:</label>
				<input type="number" name="pesel" id="pesel" value="<?=$pesel?>">
				<br>
				<label for="reg" class="text-center w-100">
					<input type="checkbox" name="reg" id="reg">
					Oświadczam, że zapoznałem się i akceptuję <a href="../regulamin/index.php" target="_blank">regulamin świadczenia usług</a>

				</label>
				<span class="error"><?= $Errreg ?></span>
				<br>
				<input type="submit" name="submit" class="send" onclick="return test()">
			</form>
		</div>
	</div>

	<script src="../js/ssm.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/form_script.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			if ("<?= $Error?>" == "Error"){
				$(".main").addClass("n-d");
				$(".section2").addClass("display");
			}
			
			$(".site-link").click(function(){
				$(".main").fadeOut(1500);
				setTimeout(function() {
					$(".section2").addClass("display");
					
				}, 100);
				
			});

			$('.call').popover('show');
			setTimeout(function(){
					$('.call').popover('hide');
				},5000);

			if($(".btn-miesz").hasClass("checked")){
				$(".no-disp").addClass("inline-display")
			};
			
			$(".miesz").click(function(){
				$(".no-disp").addClass("inline-display")
			});

			$(".dom").click(function(){
				$(".no-disp").removeClass("inline-display")
			})
		});
	</script>
	<script type="text/javascript" src="validate.js"></script>
</body>
</html>