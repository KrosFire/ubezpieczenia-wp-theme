<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DomIAuto | Regulamin</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
	<?php 
		function yearDropdown($startYear, $endYear, $id="year"){ 
		    for ($i=$startYear;$i<=$endYear;$i++){ 
		    echo "<option value=".$i.">".$i."</option>n";     
		    } 
		} 
	?>
    
</head>

<body>
<div class="container-fluid nav" id="nav">
		<nav class="navbar navbar-light navbar-expand ">
			<a href="http://domiauto.pl" class="navbar-brand">
				<img src="../img/domiauto.png" class="img-fluid brand">
			</a>
    		<div class="nav navbar-nav">
    			<a href="http://domiauto.pl" class="nav-item ml-auto">
    				<img src="../img/home.svg" alt="error" class="img-fluid home">
    			</a>	
    		</div>
    		
		</nav>
		
	</div>

	<div class="container main">
		<h1 class="w-100 text-center">Regulamin serwisu i polityka prywatności</h1>
		<h2>Administrator danych osobowych:</h2>
		<p>Administratorem Twoich danych osobowych, w związku z korzystaniem z usługi świadczonej
		przez serwis domiauto.pl, jest Domiauto.pl sp. z o.o. z siedzibą w Olsztynie (10-454) przy ul.
		Leonharda 5B, wpisana do rejestru przedsiębiorców KRS prowadzonego przez Sąd Rejonowy
		w Olsztynie, VIII Wydział Krajowego Rejestru Sądowego, pod numerem KRS 0000738589 (dalej: Administrator).
		Jeśli zawarłeś umowę ubezpieczenia, to administratorem Twoich danych osobowych jest
		towarzystwo ubezpieczeniowe, które wybrałeś.
		</p>
		<h2>Cel przetwarzania Twoich danych osobowych:</h2>
		<p>• w celu zawarcia i realizacji umowy ubezpieczenia przez towarzystwo ubezpieczeniowe (wówczas administratorem Twoich danych jest towarzystwo ubezpieczeniowe, które wybrałeś),
		<br>
		• w celu świadczenia przez Domiauto.pl sp. z o.o. usługi polegającej na zbieraniu i udostępnianiu informacji, w tym zawierających dane osobowe, od osób zainteresowanych ofertą ubezpieczeniową i przekazywaniu tych informacji agentowi pracownikowi Domiauto.pl sp. z o.o. (wówczas administratorem Twoich danych jest Domiauto.pl sp. z o.o.),
		<br>
		• w celu świadczenia przez pracownika  Domiauto.pl sp. z o.o. usługi umożliwiającej porównanie ofert i zawarcie umowy ubezpieczenia (wówczas administratorem Twoich danych jest Domiauto.pl sp. z o.o.),
		<br>
		• w celu umożliwienia kontaktu w przyszłości celem przedstawienia oferty  Domiauto.pl sp. z o.o. (wówczas administratorem Twoich danych jest Domiauto.pl sp. z o.o.),
		tj. na podstawie art. 6 ust. 1 lit. a, b i f rozporządzenia Parlamentu Europejskiego i Rady (UE)
		2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z
		przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych
		oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych).
	</p>
	<h2>Komu możemy udostępniać Twoje dane osobowe</h2>
	<p>
		Twoje dane osobowe przekazane będą towarzystwom ubezpieczeniowym i podmiotom realizującym
		płatność w związku z zawarciem umowy ubezpieczenia, które współpracują z Domiauto.pl sp.
		z o.o. Dane osobowe mogą zostać udostępnione także innym podmiotom współpracującym z Administratorem, na podstawie stosownej zgody.
	</p>
	<h2>Jak długo przechowujemy Twoje dane osobowe</h2>
	<p>Twoje dane osobowe będą przetwarzane przez Administratora i Towarzystwa
	Ubezpieczeniowe przez okres niezbędny do świadczenia Usługi lub do czasu rozwiązania
	Umowy/rezygnacji z Usługi i/lub ewentualnego zgłoszenia sprzeciwu przez osobę, której
	dane dotyczą, a po tym okresie dla celów i przez okres wymagany przez przepisy prawa lub
	dla zabezpieczenia ewentualnych roszczeń.
	</p>
	<h2>Prawo do wniesienia sprzeciwu</h2>
	<p>Przysługuje Ci prawo dostępu do treści swoich danych osobowych oraz prawo ich
sprostowania, usunięcia lub ograniczenia przetwarzania, prawo do przenoszenia danych oraz
prawo wniesienia sprzeciwu wobec ich przetwarzania, a także prawo wniesienia skargi do
organu nadzorczego.
Na podstawie wprowadzonych danych ma miejsce automatyczne podejmowanie decyzji
dotyczącej wniosku ubezpieczeniowego przez Administratora danych.
Administrator danych ocenia w sposób automatyczny czy informacje podane w formularzu
spełniają kryteria określone przez ubezpieczyciela, a w razie ich nie spełnienia, przesyła informacje zamieszczone w formularzu do innych ubezpieczycieli.
Podanie danych osobowych jest dobrowolne, ale niezbędne do realizacji usługi.
</p>
	<h2>Kontakt w sprawach związanych z przetwarzaniem danych osobowych</h2>
	<p>W kwestiach ochrony danych osobowych prosimy o kontakt pod adresem biuro@domiauto.pl</p>
	</div>







	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
</body>
</html>