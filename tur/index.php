<?php 
	include('send.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>DOMIAUTO | Formularz ubezpieczenia turystycznego</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    
</head>
<body>
	<div class="container-fluid nav" id="nav">
		<nav class="navbar navbar-light navbar-expand ">
			<a href="http://domiauto.pl" class="navbar-brand">
				<img src="../img/domiauto.png" class="img-fluid brand">
			</a>
    		<div class="nav navbar-nav">
    			<a href="http://domiauto.pl" class="nav-item ml-auto">
    				<img src="../img/home.svg" alt="error" class="img-fluid home">
    			</a>	
    		</div>
    		
		</nav>
		
	</div>
	<div class="call" data-placement="bottom" data-fallbackPlacement="counterclockwise" data-trigger="click" data-html="true" data-container=".call" data-toggle="popover" data-content="Zadzwoń do nas:<br> 
				570-752-100 <br>
				577-123-870" >
				<img src="../img/telefon.png" alt="" class="call_img" >
		</div>

	<div class="container-fluid main">


		<h1 class="welcome">
			Pokażemy Ci jak w trzech
			<br>
			prostych krokach ubezpieczyć
			<br>
			swój wyjazd
		</h1>
		<div class="list">
			<p class="one">
				1. Podaj nam swoje podstawowe dane
			</p>
			<p class="two">
				2. Prześlemy Ci 3 najlepsze oferty
			</p>
			<p class="three">
				3. Wybierz ofertę i podpisz polisę
			</p>

			<div class="submit">
				<a class="site-link" href="#section2">
						Zaczynajmy
					</a>
			</div>
		</div>
		
	</div>
	
	<div class="container-fluid section2" id="section2">

		<div class="n-d top">
			<a href="#nav" class="a-top">
				<img src="../img/top.png" class="img-fluid" alt="">
			</a>
		</div>

		

		<div class="form">
			<h1 class="tac">Wypełnij formularz:</h1>
			<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
				<label class="name tac">Strefa wyjazdu:</label>
				<?=
					$poland = $europa = $world = "";
					if($area=="Polska"){
						$poland = "checked";
					} else if($area == "Europa"){
						$europa = "checked";
					} else if($area == "Swiat"){
						$world = "checked";
					}
				?>
					<div class="container-fluid tac">
						<input type="radio" name="area" id="polska" value="Polska" class="btn-input btn-pol" <?=$poland?>>
						<label for="polska" class="zone tac">Polska</label>
						<input type="radio" name="area" id="europa" value="Europa" class="btn-input btn-euro" <?=$europa?>>
						<label for="europa" class="zone tac">Europa</label>
						<input type="radio" name="area" id="świat" value="Swiat" class="btn-input btn-world" <?=$world?>>
						<label for="świat" class="zone tac">Świat</label>
					</div>
				<label class="name tac">Rodzaj wyjazdu:</label>
				<?=
					$grupowy = $rodzinny = "";
					if($type == "indywidualny/grupowy"){
						$grupowy = "checked";
					} else if($type == "rodzinny"){
						$rodzinny = "checked";
					}
				?>
				<br>
					<input type="radio" name="grupowy/rodzinny" value="indywidualny/grupowy" id="group" class="btn-input btn-group" id="group" <?=$grupowy?>>

					<label for="group" class="group tac">
							Indywidualny / Grupowy
					</label>
					<input type="radio" name="grupowy/rodzinny" value="rodzinny" id="family" class="btn-input btn-family" id="family" <?=$rodzinny?>>
					<label for="family" class="family tac">
							Rodzinny
					</label>
					
					<div class="clearfix"></div>
					<label class="col-6 right name">Ilość osób do 26 roku życia:</label>
					<input type="number" name="underage" id="to25" value="<?=$underage?>">
					<br>
					<label class="col-6 right name">Ilość osób od 26 do 65 roku życia:</label>
					<input type="number" name="between" id="26-65" value="<?=$between?>">
					<br>	
					<label class="col-6 right name">Ilość osób powyżej 65 roku życia:</label>
					<input type="number" name="ofage" id="65after" value="<?=$ofage?>">
					<br>
					<label class="name tac">Opcje dodatkowe podróży:</label>
					<?=
						$check1 = $check2 = $check3 = $check4 = $check5 = $check6 = "";
						if($snowboard != ""){
							$check1 = "checked";
						} if($sport_ryzyko != ""){
							$check2 = "checked";
						} if($choroby != ""){
							$check3 = "checked";
						} if($praca != ""){
							$check4 = "checked";
						} if($sport_wyczyn != ""){
							$check5 = "checked";
						} if($wojna != ""){
							$check6 = "checked";
						}
					?>
					<div class="row container align-self-center mr-auto ml-auto">
						<div class="col-md-4 type">
							<input type="checkbox" class="btn-input" id="snow" value="narciarstwo/snowboarding" name="snowboard" <?=$check1?>>
							<label for="snow" class="snow tac">Narciarstwo / Snowboarding</label>
						</div>
						<div class="col-md-4 type">
							<input type="checkbox" class="btn-input" id="sport" value="sporty_ryzykowne" name="sport_ryzyko"<?=$check2?>>
							<label for="sport" class="sport tac">Sporty wysokiego ryzyka</label>
						</div>
						<div class="col-md-4 type">
							<input type="checkbox" class="btn-input" id="disease" value="choroby_przewlekle" name="choroby"<?=$check3?>>
							<label for="disease" class="disease tac">Zaostrzenie chorób przewlekłych</label>
						</div>
						<div class="col-md-4 type">
							<input type="checkbox" class="btn-input" id="work" value="praca_fizyczna" name="praca"<?=$check4?>>
							<label for="work" class="work tac">Praca fizyczna</label>
						</div>
						<div class="col-md-4 type">
							<input type="checkbox" class="btn-input" id="olimp" value="sporty_wyczynowe" name="sport_wyczyn"<?=$check5?>>
							<label for="olimp" class="olimp tac">Wyczynowe uprawianie sportu</label>
						</div>
						<div class="col-md-4 type">
							<input type="checkbox" class="btn-input" id="war" value="wojna" name="wojna"<?=$check6?>>
							<label for="war" class="war tac">Pasywny udział w wojnie</label>
						</div>
					</div>
					<label class="col-6 right name">Data rozpoczęcia:</label>
					<input type="date" name="start" id="start" value="<?=$start?>">
					<label class="col-6 right name">Data zakończenia:</label>
					<input type="date" name="end" id="end" value="<?=$finish?>">
					<h1 class="w-100 text-center">Dane Kontaktowe:</h1>
					<label for="name" class="col-6 right name">Imię:</label>
					<input type="text" name="name" id="name" value="<?=$name?>">
					<label for="surname" class="col-6 right name">Nazwisko:</label>
					<input type="text" name="surname" id="surname" value="<?=$surname?>">
					<label for="email" class="col-6 right name">E-mail:</label>
					<input type="email" name="email" id="email" value="<?=$email?>">
					<span class="error"><?= $Err2?></span>
					<label for="mobile" class="col-6 right name">Numer telefonu:</label>
					<input type="tel" name="phone" id="mobile" value="<?=$phone?>">
					<span class="error"><?= $Err2?></span>
					<label for="pesel" class="col-6 right name">PESEL:</label>
					<input type="number" name="pesel" id="pesel" value="<?=$pesel?>">
					<br>
					<label for="reg" class="text-center w-100">
					<input type="checkbox" name="reg" id="reg">
					Oświadczam, że zapoznałem się i akceptuję <a href="../regulamin/index.php" target="_blank">regulamin świadczenia usług</a>
					<span class="error"><?= $Errreg ?></span>

				</label>
					<br>
					<input type="submit" name="tur" class="send" onclick="return test()">
			</form>
		</div>
	</div>

	<script src="../js/ssm.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/form_script.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){

			if ("<?= $Error?>" == "Error"){
				$(".main").addClass("n-d");
				$(".section2").addClass("display");
			}
			$(".site-link").click(function(){
				

				$(".main").fadeOut(1500);
				setTimeout(function() {
					$(".section2").addClass("display");
					
				}, 100);
				
			});

			$('.call').popover('show');
			setTimeout(function(){
					$('.call').popover('hide');
				},5000);
			
		});
	</script>
	<script type="text/javascript" src="validate.js"></script>
</body>
</html>