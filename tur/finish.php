<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DOMIAUTO | Formularz ubezpieczenia pojazdu</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
	<?php 
		function yearDropdown($startYear, $endYear, $id="year"){ 
		    for ($i=$startYear;$i<=$endYear;$i++){ 
		    echo "<option value=".$i.">".$i."</option>n";     
		    } 
		} 
	?>
    
</head>
<body>
	<div class="container-fluid nav" id="nav">
		<nav class="navbar navbar-light navbar-expand ">
			<a href="http://domiauto.pl" class="navbar-brand">
				<img src="../img/domiauto.png" class="img-fluid brand">
			</a>
    		<div class="nav navbar-nav">
    			<a href="http://domiauto.pl" class="nav-item ml-auto">
    				<img src="../img/home.svg" alt="error" class="img-fluid home">
    			</a>	
    		</div>
    		
		</nav>
		
	</div>

	
	
	<div class="container-fluid section2 display" id="section2">
		
		<div class="n-d top">
			<a href="#nav" class="a-top">
				<img src="../img/top.png" class="img-fluid" alt="">
			</a>
		</div>

		<div class="form">
			<h2 class="w100 text-center">Formularz został wysłany! 
			<br>
			Potwierdzenie zostało wysłane na twojego maila.</h1>
		</div>
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/form_script.js"></script>

</body>
</html>