<!DOCTYPE HMTL>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />


	<title><?php bloginfo('name'); ?></title>
	<meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DOMIAUTO to Olsztyńska firma zajmująca się wyszukiwaniem najlepszych ubezpieczeń specjalnie dla Ciebie! Szukasz szybkiego ubezpieczenia? Nawet bez wychodzenia z domu? Przygotowaliśmy dla Ciebie taką możliwość wystarczy, że wejdziesz na stronę i wszystkiego się dowiesz. Zapraszamy!">
    <meta name="google-site-verification" content="Ch2iwUjSQL4PQJfHRxtCNTUQsUEEc5wbHEZmA-l6y0w" />
    <meta name="keywords" content="<?php bloginfo('tags') ?>">
    <meta name="keywords" content="domiauto, ubezpieczenia, Olsztyn, najlepsze, dom, auto, podróż">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/Owl/dist/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">

    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124419952-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-124419952-1');
	</script>
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135816830-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-135816830-1');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '386956288792793'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=386956288792793&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
	
	<!-- RICH SNIPETS -->
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Organization",
	  "url": "http://www.domiauto.pl",
	  "logo": "http://domiauto.pl/wp-content/themes/Ubezpieczenia/img/domiauto.png",
	  "name": "DOMIAUTO",
	  "description": "Najlpesze ubezpieczenia w Olsztynie",
	  "contactPoint": [{
	    "@type": "ContactPoint",
	    "telephone": "+48-570-752-100",
	    "contactType": "Customer service"
	  },{
		"@type": "ContactPoint",
		"telephone": "+48-577-123-870",
		"contactType": "Customer service"
        
	}],
	"address":{
		"@type": "PostalAddress",
		"streetAddress": "Władysława Leonharda 5B",
		"addressLocality": "Olsztyn",
		"postalCode": "10-454",
		"addressCountry": "PL"
	},
	"geo": {
		"@type": "GeoCoordinates",
		"latitude": 53.776136,
    	"longitude": 20.513247
	},
	"openingHoursSpecification": [
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday"
      ],
      "opens": "08:00",
      "closes": "16:00"
    }]
    }
	</script>

    <?php wp_head(); ?>
	
</head>
<body>
	<div class="red">
		<img src="<?php bloginfo('template_url'); ?>/img/stempel.png" class="img-fluid" alt="">
	</div>

	<div class="container-fluid">
		<nav class="navbar navbar-light navbar-expand-lg ">
    			<img src="<?php bloginfo('template_url'); ?>/img/domiauto.png" class="img-fluid brand">
    			<h1 class="brand_name">
    				Najlepsze <span class="orange">ubezpieczenia</span> w Olsztynie
    			</h1>
    			<span class="clearfix"></span>
		</nav>
		
	</div>
	<div class="gradient"></div>