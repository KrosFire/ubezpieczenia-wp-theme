		$(document).ready(function(){
			

			$(".a-top").on('click', function(e) {

                var link = $(this).attr('href');
                
                

                $('html, body').animate({
                    scrollTop: $(link).offset().top
                }, 1000); 

                e.preventDefault();

            });

            var headerheight = $('.nav').outerHeight();

				$('.main').css({
					"min-height":"calc(100vh - "+headerheight+"px)"
				});

            $(window).on('resize', function(){
				var headerheight = $('.nav').outerHeight();

				$('.main, .section2').css({
					"min-height":"calc(100vh - "+headerheight+"px)"
				});
			});

			
			
		});
			ssm.addState({
			    id: 'mobile',
			    query: '(max-width: 575px)',
			    onEnter: function(){
			    	$(window).scroll(function(){
			    		if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
			    		$(".top").removeClass('d-block');
			    	}	
			    	});
			    	        
			    }
			});

			ssm.addState({
			    id: 'Desktop',
			    query: '(min-width: 576px)',
			    onEnter: function(){
			        $(window).scroll(function(){
						if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
					    	$(".top").addClass('d-block');
						} else {
					    	$(".top").removeClass('d-block');
						}
					});
			    }
			});


			