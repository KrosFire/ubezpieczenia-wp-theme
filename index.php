<?php get_header(); ?>
		<div class="call" onclick="gtag('event', 'click', { 'event_category': 'Kliknięcie w kontakt'});" data-placement="right" data-fallbackPlacement="counterclockwise" data-trigger="click" data-html="true" data-container=".call" data-toggle="popover" data-content="Zadzwoń do nas:<br> 
				570-752-100 <br>
				577-123-870" >
				<img src="<?php bloginfo('template_url'); ?>/img/telefon.png" alt="" class="call_img" >
		</div>
		<div class="container-fluid main">
		<div class="row choice">
			<div class="col choose">
				<img src="<?php bloginfo('template_url'); ?>/img/FantastycznySamochod.svg" class="img-fluid auto">
				<h1 class="insurance">Ubezpieczenie samochodu</h1>
				<div class="submit">
				<!--
					<a class="site-link" href="<?php bloginfo('template_url'); ?>/formularz/index.php">
						Zaczynajmy
					</a>
				-->	
					<a class="site-link" href="https://domiauto.pl/ubezpieczenia-samochodu/" onclick="gtag('event', 'click', { 'event_category': 'Kliknięcie: ubezpieczenia samochodu - zaczynamy'});">
						Zaczynajmy
					</a>
				</div>
			</div>
			<div class="col choose">
				<img src="<?php bloginfo('template_url'); ?>/img/fantastycznaTurystyka.svg" class="img-fluid tour">
				<h1 class="insurance">Ubezpieczenie turystyczne</h1>
				<div class="submit">
					<a class="site-link" href="<?php bloginfo('template_url'); ?>/tur/index.php" onclick="gtag('event', 'click', { 'event_category': 'Kliknięcie: ubezpieczenia turystyczne - zaczynamy'});">
						Zaczynajmy
					</a>
				</div>
			</div>
			<div class="col choose">
				<img src="<?php bloginfo('template_url'); ?>/img/fantastycznaNieruchomosc.svg" class="img-fluid realestate">
				<h1 class="insurance">Ubezpieczenie nieruchomości</h1>
				<div class="submit">
					<a class="site-link" href="<?php bloginfo('template_url'); ?>/nieruchomości/index.php" onclick="gtag('event', 'click', { 'event_category': 'Kliknięcie: ubezpieczenia nieruchomości - zaczynamy'});">
						Zaczynajmy
					</a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container-fluid company">
    	<div class="owl-carousel owl-theme">
		    <div class="item">
				<img class="img-fluid mx-auto d-block firm" src="<?php bloginfo('template_url'); ?>/img/alianz1.png" alt="slide 1">
		    </div>
		    <div class="item">
				<img class="img-fluid mx-auto d-block firm" src="<?php bloginfo('template_url'); ?>/img/axa.png" alt="slide 2">
		    </div>
		    <div class="item">
				<img class="img-fluid mx-auto d-block firm" src="<?php bloginfo('template_url'); ?>/img/interrisk.png" alt="slide 4">
		    </div>
		    <div class="item">
				<img class="img-fluid mx-auto d-block firm" src="<?php bloginfo('template_url'); ?>/img/proama.png" alt="slide 5">
		    </div>
		    <div class="item">
				<img class="img-fluid mx-auto d-block firm" src="<?php bloginfo('template_url'); ?>/img/pzu.png" alt="slide 6">
		    </div>
		</div>
	</div>

	<div class="container-fluid QR">
		<div class="row h-100">
			<div class="col-sm-6 img-side">
				<div class="d-table-cell h-100">
					<img src="<?php bloginfo('template_url'); ?>/img/smartfonQR.png" alt="" class="scanQR">
				</div>
				
			</div>
			<div class="col-sm-6 d-table h-100">
				<div class="d-table-cell h-100">
					<p class="opisQR">Dzięki technologi QR możesz</p>
					<p class="opisQR">dużo szybciej dostać od nas</p>
					<p class="opisQR">ofertę bez wypełniania formularza</p>
					<p class="opisQR">
						<a class="" href="<?php bloginfo('template_url'); ?>/formularz/qr.php">
							<span class="btnQR">Zobacz jak</span>
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>


	<div class="container-fluid contact">
		<img src="<?php bloginfo('template_url'); ?>/img/kontakt_img.jpg" class="contact-img">
		<div class="contact-form">
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d589.3849709962482!2d20.48390746312892!3d53.77987800262355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e27ed2596986af%3A0xc1367898c6c99bc3!2sAdama+Mickiewicza+4%2C+11-041+Olsztyn!5e0!3m2!1spl!2spl!4v1545420378882" class="google-map" allowfullscreen></iframe>
				<span class="h">
					<p>
						Godziny otwarcia:
					</p>
					<p>
						Pn-Pt: 08:00 - 16:00
					</p>
					
				</span>
				<div class="clearfix"></div>
				
			</div>
			<div class="info">
				<p>Adres: ul. Mickiewicza 4			
				10-&nbsp;549&nbsp;Olsztyn</p>
				<p>Telefon: 570-752-100</p>
				<p class="second_phone">577-123-870</p>
				<p>E-mail: biuro@domiauto.pl</p>
			</div>
		</div>
	</div>
<?php get_footer() ?>
	