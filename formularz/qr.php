<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DOMIAUTO | Formularz ubezpieczenia pojazdu</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div class="container-fluid nav" id="nav">
		<nav class="navbar navbar-light navbar-expand ">
			<a href="http://domiauto.pl" class="navbar-brand">
				<img src="../img/domiauto.png" class="img-fluid brand">
			</a>
    		<div class="nav navbar-nav">
    			<a href="http://domiauto.pl" class="nav-item ml-auto">
    				<img src="../img/home.svg" alt="error" class="img-fluid home">
    			</a>	
    		</div>
    		
		</nav>
		
	</div>
	<div class="call" data-placement="bottom" data-fallbackPlacement="counterclockwise" data-trigger="click" data-html="true" data-container=".call" data-toggle="popover" data-content="Zadzwoń do nas:<br> 
				570-752-100 <br>
				577-123-870" >
				<img src="../img/telefon.png" alt="" class="call_img" >
		</div>

	<div class="container-fluid main pd">
		<div class="n-d top">
			<a href="#nav" class="a-top">
				<img src="../img/top.png" class="img-fluid" alt="">
			</a>
		</div>
		<div class="step first">
			<h1 class="hs">1. Znajdź na swoim dowodzie kod QR</h1>
			<div class="step-img first-img">
				<img class="img-fluid" src="../img/dowod_rejestracyjny.jpg"></img>
			</div>
		</div>

		<div class="step second">
			<h1 class="hs">2. Otwórz na swoim telefonie aplikację do czytania kodów QR</h1>
			<div class="step-img second-img">
				<img class="img-fluid" src="../img/scan.png"></img>
			</div>
		</div>

		<div class="step third">
			<h1 class="hs">3. Zeskanuj kod z dowodu i wyślij nam mailem razem z <span class="orange">
				numerem telefonu:
			</span></h1>
			<h2 class="hs">biuro@domiauto.pl</h2>
			<div class="step-img third-img">
				<img class="img-fluid" src="../img/WysylQR.png"></img>
			</div>
			<div class="logo-img">
				<img src="../img/domiauto.png" alt="" class="img-fluid">
			</div>

		</div>
		
	</div>

	<script src="../js/ssm.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/form_script.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			$('.call').popover('show');
			setTimeout(function(){
					$('.call').popover('hide');
				},5000);

		});
	</script>
</body>
</html>