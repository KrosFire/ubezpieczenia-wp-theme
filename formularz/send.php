<?php 
	$Error = $Errmarka = $Errmodel = $Errnadowize = $Errpaliwo = $Errprodukcja = $Errpojemnosc = $Errmoc = $Errnumerrej = $Errprzebieg = $Errdatarej = $Errvin = $Erruzytek = $Errdrzwi = $Errname = $Errsurname = $Errpesel = $Errpost = $Errprawko = $Errname1 = $Errsurname1 = $Errpesel1 = $Errpost1 = $Errprawko1 = $Errname2 = $Errsurname2 = $Errpesel2 = $Errpost2 = $Errprawko2 = $Errstartubez = $Errreg = $ocacErr = $Err2 = "";
	$ac = $oc = $marka = $model = $nadwozie = $paliwo = $rok_produkcji = $pojemnosc = $moc = $numer_rej = $przebieg = $pierwsza_rej = $VIN = $uzytek = $ilosc_drzwi = $imie = $nazwisko = $email = $phone = $pesel = $kod_pocztowy = $rok_prawka = $wspolwlasc = $imie_owner1 = $nazwisko_owner1 = $pesel_owner1 = $kod_pocztowy_owner1 = $rok_prawka_owner1 = $imie_owner2 = $nazwisko_owner2 = $pesel_owner2 = $kod_pocztowy_owner2 = $rok_prawka_owner2 = $data_rozpoczecia = "";

	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		
		if(empty($_POST['oc']) && empty($_POST['ac'])){
			$ocacErr = "Wybierz jedną z opcji";
		} else if(empty($_POST['oc'])){
			$ac = test_input($_POST['ac']);
		} else if(empty($_POST['ac'])){
			$oc = test_input($_POST['oc']);
		} else{
			$ac = test_input($_POST['ac']);
			$oc = test_input($_POST['oc']);
		}

		if($_POST['brand'] == ""){
			$Errmarka = "Wypełnij to pole";
		} else{
			$marka = test_input($_POST['brand']);
		}

		if(empty($_POST['model'])){
			$Errmodel = "Wypełnij to pole";
		} else{
			$model = test_input($_POST['model']);
		}

		if(empty($_POST['nadwozie'])){
			$Errnadowize = "Wypełnij to pole";
		} else{
			$nadwozie = test_input($_POST['nadwozie']);
		}

		if($_POST['fuel'] == ""){
			$Errpaliwo = "Wypełnij to pole";
		} else{
			$paliwo = test_input($_POST['fuel']);
		}

		if($_POST['year'] == ""){
			$Errprodukcja = "Wypełnij to pole";
		} else{
			$rok_produkcji = test_input($_POST['year']);
		}

		if(empty($_POST['capicity'])){
			$Errpojemnosc = "Wypełnij to pole";
		} else{
			$pojemnosc = test_input($_POST['capicity']);
		}

		if(empty($_POST['power'])){
			$Errmoc = "Wypełnij to pole";
		} else{
			$moc = test_input($_POST['power']);
		}

		if(empty($_POST['number'])){
			$Errnumerrej = "Wypełnij to pole";
		} else{
			$numer_rej = test_input($_POST['number']);
		}

		if(empty($_POST['dystans'])){
			$Errprzebieg = "Wypełnij to pole";
		} else{
			$przebieg = test_input($_POST['dystans']);
		}

		if(empty($_POST['first_reg'])){
			$Errdatarej = "Wypełnij to pole";
		} else{
			$pierwsza_rej = test_input($_POST['first_reg']);
		}

		if(empty($_POST['VIN'])){
			$Errvin = "Wypełnij to pole";
		} else{
			$VIN = test_input($_POST['VIN']);
		}

		if($_POST['use'] == ""){
			$Erruzytek = "Wypełnij to pole";
		} else{
			$uzytek = test_input($_POST['use']);
		}

		if(empty($_POST['doors'])){
			$Errdrzwi = "Wypełnij to pole";
		} else{
			$ilosc_drzwi = test_input($_POST['doors']);
		}

    	if (empty($_POST["name"])) {
	    	$Errname = "Wypełnij to pole";
	  	} else {
	    	$imie = test_input($_POST["name"]);
	  	}

	  	if(empty($_POST['surname'])){
			$Errsurname = "Wypełnij to pole";
		} else{
			$nazwisko = test_input($_POST['surname']);
		}

	  if (empty($_POST["email"]) && empty($_POST['phone'])) {
	    $Err2 = "Wypełnij jedno z tych pól";
	  	} else if(empty($_POST["email"])){
	    
	    $phone = test_input($_POST['phone']);
	  	}else if(empty($_POST["phone"])){
	  		$email = $_POST["email"];

	  	} else{
	  		$email = $_POST["email"];
	  		$phone = test_input($_POST['phone']);
	  	}

	  if (empty($_POST["pesel"])) {
	    $Errpesel = "Wypełnij to pole";
	  } else {
	    $pesel = test_input($_POST["pesel"]);
	  }

	  if (empty($_POST["post"])) {
	    $Errpost = "Wypełnij to pole";
	  } else {
	    $kod_pocztowy = test_input($_POST["post"]);
	  }

	  if ($_POST["pass_year"] == "") {
	    $Errprawko = "Wypełnij to pole";
	  } else {
	    $rok_prawka = test_input($_POST["pass_year"]);
	  }

	  if ($_POST["coowners"] == "0") {
	    $wspolwlasc = test_input($_POST['coowners']);
	  } else if ($_POST["coowners"] == "1"){
	  	$wspolwlasc = test_input($_POST['coowners']);    	
		    if (empty($_POST["name_owner1"])) {
		    	$Errname1 = "Wypełnij to pole";
		  	} else {
		    	$imie_owner1 = test_input($_POST["name_owner1"]);
		  	}

		  	if(empty($_POST['surname_owner1'])){
				$Errsurname1 = "Wypełnij to pole";
			} else{
				$nazwisko_owner1 = test_input($_POST['surname_owner1']);
			}

		  if (empty($_POST["pesel_owner1"])) {
		    $Errpesel1 = "Wypełnij to pole";
		  } else {
		    $pesel_owner1 = test_input($_POST["pesel_owner1"]);
		  }

		  if (empty($_POST["post_owner1"])) {
		    $Errpost1 = "Wypełnij to pole";
		  } else {
		    $kod_pocztowy_owner1 = test_input($_POST["post_owner1"]);
		  }

		  if ($_POST["pass_year_owner1"] == "") {
		    $Errprawko1 = "Wypełnij to pole";
		  } else {
		    $rok_prawka_owner1 = test_input($_POST["pass_year_owner1"]);
		  }
	  } else if ($_POST["coowners"] == "2"){

	    	$wspolwlasc = test_input($_POST['coowners']);

	    	if (empty($_POST["name_owner1"])) {
		    	$Errname1 = "Wypełnij to pole";
		  	} else {
		    	$imie_owner1 = test_input($_POST["name_owner1"]);
		  	}

		  	if(empty($_POST['surname_owner1'])){
				$Errsurname1 = "Wypełnij to pole";
			} else{
				$nazwisko_owner1 = test_input($_POST['surname_owner1']);
			}

		  if (empty($_POST["pesel_owner1"])) {
		    $Errpesel1 = "Wypełnij to pole";
		  } else {
		    $pesel_owner1 = test_input($_POST["pesel_owner1"]);
		  }

		  if (empty($_POST["post_owner1"])) {
		    $Errpost1 = "Wypełnij to pole";
		  } else {
		    $kod_pocztowy_owner1 = test_input($_POST["post_owner1"]);
		  }

		  if ($_POST["pass_year_owner1"] == "") {
		    $Errprawko1 = "Wypełnij to pole";
		  } else {
		    $rok_prawka_owner1 = test_input($_POST["pass_year_owner1"]);
		  }
	    	if (empty($_POST["name_owner2"])) {
		    	$Errname2 = "Wypełnij to pole";
		  	} else {
		    	$imie_owner2 = test_input($_POST["name_owner2"]);
		  	}

		  	if(empty($_POST['surname_owner2'])){
				$Errsurname2 = "Wypełnij to pole";
			} else{
				$nazwisko_owner2 = test_input($_POST['surname_owner2']);
			}

		  if (empty($_POST["pesel_owner2"])) {
		    $Errpesel2 = "Wypełnij to pole";
		  } else {
		    $pesel_owner2 = test_input($_POST["pesel_owner2"]);
		  }

		  if (empty($_POST["post_owner2"])) {
		    $Errpost2 = "Wypełnij to pole";
		  } else {
		    $kod_pocztowy_owner2 = test_input($_POST["post_owner2"]);
		  }

		  if ($_POST["pass_year_owner2"] == "") {
		    $Errprawko2 = "Wypełnij to pole";
		  } else {
		    $rok_prawka_owner2 = test_input($_POST["pass_year_owner2"]);
		  }

		}

	  if ($_POST["start_year"] == "") {
	    $Errstartubez = "Wypełnij to pole";
	  } else {
	    $data_rozpoczecia = test_input($_POST["start_year"]);
	  }

	  if (empty($_POST["regulamin"])) {
	    $Errreg = "Wypełnij to pole";
	  }



	  if($Errreg == "" and $Err2 == "" ){
	  	$to = "biuro@domiauto.pl";
	  	$subject = "Zlecenia Formularz OC/AC";
	  	$subject=iconv("UTF-8","ISO-8859-2", $subject);

	  	if($ac == ""){
	  		$ac = "nie";
	  	}
	  	if($oc == ""){
	  		$oc = "nie";
	  	}
	  	
		$message =  "<html><head></head><body>OC:" . $oc . " AC:" . $ac . "<br> Marka: " . $marka . "<br> Model samochodu: " . $model . "<br> Rodzaj nadwozia: " . $nadwozie . "<br> Rodzaj paliwa: " . $paliwo . "<br> Rok produkcji: " . $rok_produkcji . "<br> Pojemność silnika: " . $pojemnosc . "<br> Numer rejestracyjny: " . $numer_rej . "<br> Przebieg: " . $przebieg . " <br>Data pierwszej rejestacji: " . $pierwsza_rej . "<br> Numer VIN: " . $VIN . "<br> Rodzaj użytkowania: " . $uzytek . "<br> Ilość drzwi: " . $ilosc_drzwi . "<br> Imię: " . $imie . "<br> Nazwisko:" . $nazwisko . "<br> E-mail: " . $email . "<br> PESEL:" . $pesel . "<br> Numer telefonu:" . $phone . "<br> Kod pocztowy: " . $kod_pocztowy . "<br> Rok uzyskania prawa jazdy: " . $rok_prawka . "<br> Ilość współwłaścicieli: " . $wspolwlasc . "<br> Data rozpoczęcia OC: " . $data_rozpoczecia ;
	  	
	  	if($wspolwlasc == "1"){
	  		$message .= "<br>Imię Współwłaściciela: " . $imie_owner1 . "<br> Nazwisko Współwłaściciela: " . $nazwisko_owner1 . " <br>PESEL Współwłaściciela: " . $pesel_owner1 . "<br> Kod pocztowy: " . $kod_pocztowy_owner1 . "<br> Rok uzyskania prawa jazdy: " . $rok_prawka_owner1 ;
	  	}

	  	if($wspolwlasc == "2"){
	  		$message .= "<br> Imię Współwłaściciela: " . $imie_owner2 . "<br> Nazwisko Współwłaściciela: " . $nazwisko_owner2 . " <br>PESEL Współwłaściciela: " . $pesel_owner2 . "<br> Kod pocztowy: " . $kod_pocztowy_owner2 . "<br> Rok uzyskania prawa jazdy: " . $rok_prawka_owner2 ;
	  	}

	  	$message .= "</body></html>";
	    
	    $message2 = "Dziękujemy za wypełnienie formularza!
	    <br>
		Jeden z naszych agentów już zajmuje się Pana/Pani ubezpieczeniem. 
		<br>
                         Miłego dnia życzy domiauto.pl";
	    $from = $email;

	    $header = "From: " . $from;
	    $header.='MIME-Version: 1.0' . "\r\n";
		$header.='Content-type: text/html; charset=UTF-8' . "\r\n";

	    $header2 = "From: " . $to;
	    $header2 .='MIME-Version: 1.0' . "\r\n";
		$header2 .='Content-type: text/html; charset=UTF-8' . "\r\n";
	    mail($to,$subject,$message,$header);
	    mail($from,$subject,$message2,$header2); // sends a copy of the message to the sender
	    //echo $message;
	    
	    $servername = "192.168.101.134";
		$username = "domiauto_root";
		$password = "Q@wertyuiop";
		$dbname = "domiauto_clients";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		$conn -> query ('SET NAMES utf8');
		$conn -> query ('SET CHARACTER_SET utf8_unicode_ci');
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 

		$sql = "INSERT INTO klienci (Imie, Nazwisko, Email, Telefon)
		VALUES ('".$imie."', '".$nazwisko."', '".$email."', '".$phone."')";

		if ($conn->query($sql) === TRUE) {
		    echo "New record created successfully";
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();


	    
	    header('Location: finish.php');
	  }else{
	  	$Error = "Error";
	  }

	} 
?>