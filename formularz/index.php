<?php 
include('send.php');
require "dbconnect.php";

$sql = "SELECT name FROM car_make";

$result = mysqli_query($conn, $sql) or die("BAD SQL: $sql");

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DOMIAUTO | Formularz ubezpieczenia pojazdu</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
	<?php 
		function yearDropdown($startYear, $endYear, $id="year", $wybrany_rok = ""){ 
		    for ($i=$startYear;$i>=$endYear;$i--){ 
		    	$select_year ="";
		    	if($wybrany_rok == $i){
		    		$select_year = "selected";
		    	}
		    echo "<option value=".$i." {$select_year}>".$i."</option>";     
		    } 
		} 
	?>
    
</head>
<body>
	<div class="left-slide">
		<div class="top-slide">
			<span class="arrow" id="arrow">&#x2190;</div>
		
		<h5>Dzięki technologi QR możesz pominąć formularz</h5>
		<p class="w-100 text-center">
			<a href="qr.php">Zobacz jak!</a>
		</p>
		
	</div>

	<div class="container-fluid nav" id="nav">
		<nav class="navbar navbar-light navbar-expand ">
			<a href="http://domiauto.pl" class="navbar-brand">
				<img src="../img/domiauto.png" class="img-fluid brand">
			</a>
    		<div class="nav navbar-nav">
    			<a href="http://domiauto.pl" class="nav-item ml-auto">
    				<img src="../img/home.svg" alt="error" class="img-fluid home">
    			</a>	
    		</div>
    		
		</nav>
		
	</div>
	<div class="call" data-placement="bottom" data-fallbackPlacement="counterclockwise" data-trigger="click" data-html="true" data-container=".call" data-toggle="popover" data-content="Zadzwoń do nas:<br> 
				570-752-100 <br>
				577-123-870" >
				<img src="../img/telefon.png" alt="" class="call_img" >
		</div>

	<div class="container-fluid main">
		<h1 class="welcome">
			Pokażemy Ci jak w trzech
			<br>
			prostych krokach ubezpieczyć
			<br>
			swój samochód
		</h1>
		<div class="list">
			<p >
				1. Podaj nam swoje podstawowe dane
			</p>
			<p >
				2. Prześlemy Ci 3 najlepsze oferty
			</p>
			<p >
				3. Wybierz ofertę i podpisz polisę
			</p>

			<div class="submit">
				<a class="site-link" href="#section2">
						Zaczynajmy
					</a>
			</div>
		</div>

		
	</div>
	
	<div class="container-fluid section2" id="section2">
		
		<div class="n-d top">
			<a href="#nav" class="a-top">
				<img src="../img/top.png" class="img-fluid" alt="">
			</a>
		</div>

		

		<div class="form">
			<h1>Wypełnij formularz:</h1>
			<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
				<?= 
				$checkoc = $checkac = "";
				if($oc != ""){
						$checkoc = "checked";
					} if( $ac != ""){
						$checkac = "checked";
					}
				?>
				<input type="checkbox" name="oc" id="oc" value="tak" class="btn-input btn-oc" <?= $checkoc?> />
				<label class="oc" for="oc" >
					<h1>OC</h1>
				</label>
				<input type="checkbox" name="ac" id="ac" value="tak" class="btn-input btn-ac" 
				<?= $checkac?>/>
				<label class="ac" for="ac"  >
					<h1>AC</h1>
				</label>
				
				<div class="clearfix"></div>
				<label for="marka" class="btn col-6 right name">Wybierz markę:</label>
				<select name="brand" id="marka">
					<option value="">--Wybierz--</option>
					<?php 
						while($row = mysqli_fetch_assoc($result)){
							$selected="";
							if($row['name'] == $marka){
								$selected = "selected";
							}
							$opt = "<option value='{$row['name']}' {$selected}>{$row['name']}</option>";
							echo $opt;
						}
					?>
					
				</select>
				<label for="model" class="col-6 right name">Wpisz model samochodu:</label>
				<input type="text" name="model" id="model" value="<?= $model ?>">
				<label class="col-6 right name">Rodzaj nadwozia:</label>
				<input type="text" name="nadwozie" id="nadw" value="<?= $nadwozie ?>">
				<label class="col-6 right name">Rodzaj paliwa:</label>
				<select name="fuel" id="fuel">
					<?php $benzyna = "benzyna";
						$gaz = "benzyna/gaz";
						$diesel = "diesel";
						$check1 = $check2 =$check3 ="";
						if($paliwo == $benzyna){
							$check1 = "selected";
						} else if($paliwo == $gaz){
							$check2 = "selected";
						} else if($paliwo == $diesel){
							$check3 = "selected";
						}
					 ?>
					<option value="">--wybierz--</option>
					<option value="benzyna" <?= $check1 ?>>benzyna</option>
					<option value="benzyna/gaz" <?= $check2?>>benzyna/gaz</option>
					<option value="diesel" <?= $check3 ?>>diesel</option>
				</select>
 				<label for="rok" class="col-6 right name">Rok produkcji:</label>
				<select type="text" name="year" id="rok">
					<option value="">--wybierz--</option>
					<?php 
						yearDropdown(date('Y'),1950,"year",$rok_produkcji);
					?>
				</select>
				<label for="pojemnosc" class="col-6 right name">Pojemność silnika:</label>
				<input type="text" name="capicity" id="pojemnosc" value="<?= $pojemnosc ?>">
				<label class="col-6 right name">Moc: (kW):</label>
				<input type="number" name="power" id="power" value="<?= $moc ?>">
				<label for="rejestracja" class="col-6 right name">Numer rejestracyjny:</label>
				<input type="text" name="number" id="rejestracja" value="<?= $numer_rej ?>">
				<label for="przebieg" class="col-6 right name">Przebieg (km):</label>
				<input type="number" id="przebieg" name="dystans" value="<?= $przebieg ?>">
				<label class="col-6 right name">Data pierwszej rejestracji:</label>
				<input type="date" name="first_reg" id="first_reg" value="<?= $pierwsza_rej ?>">
				<label class="col-6 right name">Numer VIN:</label>
				<input type="text" name="VIN" id="VIN" value="<?= $VIN ?>">
				<label class="col-6 right name">Rodzaj użytkowania:</label>
				<select name="use" id="use">
					<?=
						$uzytek1 = $uzytek2 = $uzytek3 = $uzytek4 ="";
						if($uzytek=="zwykłe"){
							$uzytek1 = "selected";
						} else if($uzytek == "nauka"){
							$uzytek2="selected";
						} else if($uzytek == "taxi"){
							$uzytek3="selected";
						} else if($uzytek == "transport"){
							$uzytek4="selected";
						} 
					?>
					<option value="">--wybierz--</option>
					<option value="zwykłe" <?= $uzytek1?>>Zwykłe</option>
					<option value="nauka" <?= $uzytek2?>>Nauka jazdy</option>
					<option value="taxi" <?= $uzytek3?>>Taxi</option>
					<option value="transport" <?= $uzytek4?>>Przewóz towarów</option>
				</select>
				<label class="col-6 right name">Ilość drzwi:</label>
				<input type="number" name="doors" id="doors" value="<?= $ilosc_drzwi ?>">
				<h1>Dane Właściciela:</h1>	
				<label for="name" class="col-6 right name">Imię:</label>
				<input type="text" name="name" id="name" value="<?= $imie ?>">
				<label for="surname" class="col-6 right name">Nazwisko:</label>
				<input type="text" name="surname" id="surname" value="<?= $nazwisko ?>">
				
				<label for="pesel" class="col-6 right name">PESEL:</label>
				<input type="number" name="pesel" id="pesel" value="<?= $pesel ?>">
				<label class="col-6 right name">Kod pocztowy:</label>
					<input type="text" name="post" id="post" placeholder="__-___" value="<?= $kod_pocztowy ?>">
				<label class="col-6 right name">Rok uzyskania prawa jazdy:</label>
				<select name="pass_year" id="pass_year">
					<option value="">--wybierz--</option>
					<?php 
						yearDropdown(date('Y'),1950,"year",$rok_prawka);
					?>
				</select>
				<label class="col-6 right name">Ilość współwłaścicieli pojazdu:</label>
					<select name="coowners" id="owner">
						<?=
							$jeden = $dwa = $trzy = "";
							if($wspolwlasc == "0"){
								$jeden = "selected";
							} else if($wspolwlasc == "1"){
								$dwa = "selected";
							} else if($wspolwlasc == "2"){
								$trzy = "selected";
							}
						?>
						<option value="0" <?= $jeden ?>>Brak</option>
						<option value="1" <?= $dwa ?>>1</option>
						<option value="2" <?= $trzy ?>>2</option>
					</select>
				<br>
				
				<label class="col-6 right name">Data rozpoczęcia ubezpieczenia</label>
				<input type="date" id="startYear" name="start_year" value="<?= $data_rozpoczecia ?>">
				<label for="email" class="col-6 right name">E-mail:</label>
				<input type="email" name="email" id="email" value="<?= $email ?>">
				<span class="error"><?= $Err2 ?></span>
				<label for="mobile" class="col-6 right name">Numer telefonu:</label>
				<input type="tel" name="phone" id="mobile" value="<?= $phone ?>">
				<span class="error"><?= $Err2 ?></span>
				<span class="n-d one">
					<h1>Dane Współwłaściciela:</h1>	
					<label for="name" class="col-6 right name">Imię:</label>
					<input type="text" name="name_owner1" id="name" value="<?= $imie_owner1 ?>">
					<label for="surname" class="col-6 right name">Nazwisko:</label>
					<input type="text" name="surname_owner1" id="surname" value="<?= $nazwisko_owner1 ?>">
					<label for="pesel" class="col-6 right name">PESEL:</label>
					<input type="number" name="pesel_owner1" id="pesel" value="<?= $pesel_owner1 ?>">
					<label class="col-6 right name">Kod pocztowy:</label>
					<input type="text" name="post_owner1" placeholder="__-___" value="<?= $kod_pocztowy_owner1 ?>">
					<label class="col-6 right name">Rok uzyskania prawa jazdy:</label>
					<select name="pass_year_owner1">
						<option>--wybierz--</option>
						<?php 
							yearDropdown(date('Y'),1950,"year",$rok_prawka_owner1);
						?>
					</select>
				</span>
				<br>
				<span class="n-d two">
					<h1>Dane Współwłaściciela:</h1>	
					<label for="name" class="col-6 right name">Imię:</label>
					<input type="text" name="name_owner2" id="name" value="<?= $imie_owner2 ?>">
					<label for="surname" class="col-6 right name">Nazwisko:</label>
					<input type="text" name="surname_owner2" id="surname" value="<?= $nazwisko_owner2 ?>">
					<label for="pesel" class="col-6 right name">PESEL:</label>
					<input type="number" name="pesel_owner2" id="pesel" value="<?= $pesel_owner2 ?>">
					<label class="col-6 right name">Kod pocztowy:</label>
					<input type="text" name="post_owner2" placeholder="__-___" value="<?= $kod_pocztowy_owner2 ?>">
					<label class="col-6 right name">Rok uzyskania prawa jazdy:</label>
					<select name="pass_year_owner2">
						<option>--wybierz--</option>
						<?php 
							yearDropdown(date('Y'),1950,year,$rok_prawka_owner2);
						?>
					</select>
				</span>
				<br>
				
				<label for="reg" class="text-center w-100">
					<input type="checkbox" name="regulamin" id="reg">
					Oświadczam, że zapoznałem się i akceptuję <a href="../regulamin/index.php" target="_blank">regulamin świadczenia usług</a>

				</label><span class="error"><?= $Errreg?></span>
				<br>
				<br>
				<input type="submit" name="submit" class="send" onclick="return test()" />
			</form>

		</div>
	</div>

	<script src="../js/ssm.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/form_script.js"></script>
	<script type="text/javascript">
		
		$(document).ready(function(){
			
    	
			$('.arrow').click(function(){
				if($('#arrow').hasClass('hidden-arrow') == true){
					$('.left-slide').removeClass('hidden');
					$('.arrow').removeClass('hidden-arrow');
				}else{
					$('.left-slide').addClass('hidden');
					$('.arrow').addClass('hidden-arrow');
				}
				
			});
			

			if("<?= $Error?>" == "Error"){
				$(".main").addClass("n-d");
				$(".section2").addClass("display")
			}

			$(".site-link").click(function(){
				$(".main").fadeOut(1500);
				setTimeout(function() {
					$(".section2").addClass("display")
					
				}, 100);
				
			});

			$('.call').popover('show');
			setTimeout(function(){
					$('.call').popover('hide');
				},5000);

			$("#owner").ready(function(){
				const select = document.querySelector('#owner');
				var val = (select.options[select.selectedIndex].value)
				if (val == 1) {
					$('.one').addClass('display');
					$('.two').removeClass('display');
				}

				if (val == 2) {
					$('.one, .two').addClass('display')
				}

				if (val == 0) {
					$('.one').removeClass('display');
					$('.two').removeClass('display');
				}

			});

			$("#owner").change(function(){
				const select = document.querySelector('#owner');
				var val = (select.options[select.selectedIndex].value)
				if (val == 1) {
					$('.one').addClass('display');
					$('.two').removeClass('display');
				}

				if (val == 2) {
					$('.one, .two').addClass('display')
				}

				if (val == 0) {
					$('.one').removeClass('display');
					$('.two').removeClass('display');
				}

			});
		});
	</script>
	<script type="text/javascript" src="validate.js"></script>
</body>
</html>