<div class="footer">
		<h1 class="creator">Wszystkie prawa zastrzeżone &copy;&nbsp;2018&nbsp;DOMIAUTO. Twórcy:&nbsp;mhdesign.kontakt@gmail.com</h1>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/Owl/dist/owl.carousel.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.call').popover('show');
			setTimeout(function(){
					$('.call').popover('hide');
			},5000);
		});
	
	var owl = $('.owl-carousel');
		owl.owlCarousel({
		    items:4,
		    loop:true,
		    autoplay:true,
		    autoplayTimeout:5000,
		    autoplayHoverPause:false,
		    smartSpeed: 750,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            margin: 10050,
		            smartSpeed: 550
		        },
		        576:{
		            items:3,
		            margin: 90
		        },
		        768:{
		            items:3,
		            margin: 100
		        },

		        920:{
		            items:4,
		            margin: 90
		        },

		        1200:{
		            items:4,
		            margin: 210

		        }
		    }

		});
	</script>

	<!-- BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
	(function(){ var widget_id = 'MtZM1cFn6q';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
	</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>